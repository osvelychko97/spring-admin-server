FROM adoptopenjdk/openjdk11:latest
COPY ./target/spring-admin-server-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
RUN sh -c 'touch spring-admin-server-0.0.1-SNAPSHOT.jar'
ENTRYPOINT ["java","-jar","spring-admin-server-0.0.1-SNAPSHOT.jar"]