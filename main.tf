variable "region" {
  default = "eu-west-2"
}

provider "aws" {
  access_key = "AKIA3EK5LNHBE4KTQZ3G"
  secret_key = "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"
  region = var.region
}

resource "aws_instance" "linux_instance" {
  ami = "ami-04a26b5fb882a2eb2"
  instance_type = "t2.micro"
  key_name = "aws_key"
  vpc_security_group_ids = [
    aws_security_group.linux_instance.id]

  user_data = "${file("app-cloud-config.yaml")}"

  tags = {
    Name = "Spring admin server"
  }
}

resource "aws_security_group" "linux_instance" {
  name = "myCustomSecurityGroup"
  description = "my linux SecurityGroup"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    from_port = 8888
    to_port = 8888
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    from_port = 9000
    to_port = 9000
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    from_port = 19531
    to_port = 19531
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  tags = {
    Name = "linux instance"
  }
}